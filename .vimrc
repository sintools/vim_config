" Main options
set number
syntax on
filetype plugin indent on
set mouse=a
set nocompatible
set ts=4           "tab = 4 spaces
set softtabstop=4 shiftwidth=4 expandtab   "wstawia 4 spacje zamiast taba
set nowrap         "noline wrape
set cindent shiftwidth=4
set cursorline
set wildmenu
set noswapfile
set hlsearch
set listchars+=trail:.
set autoread

let g:solarized_termcolors=256
" colorscheme solarized
colorscheme tender
" Omni completition settings
let OmniCpp_SelectFirstItem = 2 " select first item (but don't insert)
let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
let OmniCpp_MayCompleteDot = 0 " autocomplete after .
let OmniCpp_MayCompleteArrow = 0 " autocomplete after ->
let OmniCpp_MayCompleteScope = 0 " autocomplete after ::
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
set completeopt=menuone,menu,longest

"CtrlP
let g:ctrlp_regexp = 1 "regexp serch by default
let g:ctrlp_map = '<c-f>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_extensions = ['tag']
" Cplane
let g:CPlaneTagsComponents = ["rrom", "enbc","cellc","pit","uec"]
map <F3> :call CPlaneJumpHeaderSource()<CR>

" auto remove trailing whitespaces
autocmd BufWritePre * :%s/\s\+$//e

" NERDTree
let NERDTreeIgnore = ['\.pyc$']
let NERDTreeShowBookmarks=1
nmap <silent> <F5> :NERDTreeToggle<CR>

" Airline
set laststatus=2   "airline always on
let g:airline_theme = "tender"

" File extension ignore in search
set wildignore=.svn,CVS,.git,*.o,*.a,*.class,*.mo,*.la,*.so,*.obj,*.swp,*.jpg,*.png,*.xpm,*.gif,*.pyc
command RemoveTrash %s/^.*FCT-\d011-\d\+-//
command MakeThis make '%:r' CXXFLAGS+="-pthread -std=c++14 -Wall" | ! ./'%:r'

autocmd FileType c,cpp,hpp,h ClangFormatAutoEnable

nmap <silent> <F6> :split<CR>
nmap <silent> <F7> :vsplit<CR>

let g:plug_shallow = 0


